import java.util.*;

public class Puzzle {

   public static ArrayList<String> viited = new ArrayList<>();
   public static String[] split;
   public static int counter = 0;
   public static Set<String> set = new HashSet<>();


   /** Solve the word puzzle.
    * @param args three words (addend1 addend2 sum)
    */
   public static void main (String[] args) {
      viited.clear();
      split = null;
      counter = 0;
      set.clear();

      //solvePuzzle("SEND MORE MONEY");
      //solvePuzzle("YKS KAKS KOLM");
      //solvePuzzle("ABCDEFGHIJAB ABCDEFGHIJA ACEHJBDFGIAC");
      //runConsole();

      StringBuilder temp = new StringBuilder();
      for (String s : args){
         temp.append(s);
         temp.append(" ");
      }
      solvePuzzle(temp.toString());
   }


   /**
    * Method for handling console interaction and neccessary calls to other functions.
    */
   public static void runConsole(){
      Scanner reader = new Scanner(System.in);
      System.out.println("Want to play a little game? Enter three words in uppercase!");
      String input = reader.nextLine();
      System.out.println("Processing input...");

      solvePuzzle(input);
   }

   /**
    * Method that handles puzzle solving by input string.
    * @param str
    */
   public static void solvePuzzle(String str) {
      long starttime;
      long endtime;
      long timediff;

      starttime = System.nanoTime();

      isValidInput(str);
      String viitedhelper = str.replaceAll("\\s","");
      char[] chars = viitedhelper.toCharArray();

      for (char s : chars){
         if (!viited.contains(Character.toString(s))){
            viited.add(Character.toString(s));
            viited.add(null); //placeholder for numbers matching
         }
      }

      if (viited.size()/2 > 10) throw new RuntimeException("There were more than 10 individual elements in entered expression " + str + " could not solve puzzle!");

      split = str.trim().split("\\s+");

      permutation("1234567890", "", viited.size()/2);
      for (String s : set){
         matchWithMap(s.toCharArray());
      }

      endtime = System.nanoTime();
      timediff = ((endtime-starttime)/1000000);
      System.out.println("Puzzle solver execution time was : " + timediff + " ms");

      System.out.println("There were " + counter + " answers!");
   }


   /**
    * A method that takes the "viited" arraylist with unique letters(A,B,C...etc) at even positions. Then it matches elements from a random permutation "numbers" to odd positions.
    * Needs to be called once with every permutation of numbers 0-9.
    * @param numbers
    */
   public static void matchWithMap (char[] numbers){
      ArrayList<String> helper = (ArrayList<String>) viited.clone(); //kloonib varasema arraylisti, et seda ei peaks pidevalt uuesti leidma.

      int numbershelper = 0; // Helper variable for the following cycle.

      for (int i = 0; i < helper.size(); i++) { //this puts elements from "numbers" intp "helper"s odd positions one by one.
         if (!((i % 2) == 0)) {
            helper.set(i, Character.toString(numbers[numbershelper]));
            numbershelper ++;
         }
      }

      StringBuilder added1 = new StringBuilder(); //Here a string of numbers is concatenated matching the 1st word.
      StringBuilder added2 = new StringBuilder(); //Here a string of numbers is concatenated matching the 2nd word.
      StringBuilder sum = new StringBuilder(); //Here a string of numbers is concatenated matching the answer word.

      char[] chars = split[0].toCharArray();
      for (char c : chars){
         added1.append(helper.get(helper.indexOf(Character.toString(c))+1)); // this asks "viited" for index of A, then adds to the stringbuilder the value that is in "viited" one step AFTER A. The number value we are actually looking for.
      }

      char[] chars2 = split[1].toCharArray();
      for (char c : chars2){
         added2.append(helper.get(helper.indexOf(Character.toString(c))+1));
      }

      char[] chars3 = split[2].toCharArray();
      for (char c : chars3){
         sum.append(helper.get(helper.indexOf(Character.toString(c))+1));
      }

      if (!(added1.toString().substring(0,1).equals("0")|| added2.toString().substring(0,1).equals("0") ||sum.toString().substring(0,1).equals("0"))){ //checks if any of the built strings using this permutation have zeros in the beginning. If they do, the whole operation is discarded.

         if ((Long.parseLong(added1.toString()) + Long.parseLong(added2.toString())) == Long.parseLong(sum.toString())) { //If no zeros at beginnings, it checks if the equation a + b = c is true;
            System.out.println(added1 + " + " + added2 + " = " + sum); //If true, print out equation;
            counter ++; //If true, increment counter;
         }
      }
   }


   /**
    * Sources used for the permutations function:
    * //http://www.codingeek.com/java/strings/find-all-possible-permutations-of-string-using-recursive-method/
    * Its purpose is to generate all permutations of a given string in the manner that the no permutation has in itselt concurrent numbers.
    * howmany notes the amount of individual letters in our three word expression in the start;
    * sofar is a helper string neccessary for recursion.
    * input is the string we will be finding recursions for.
    * @param input
    * @param sofar
    * @param howmany
    */
   private static void permutation(String input, String sofar, Integer howmany) {
      if (input.equals("")) {
         set.add(sofar.substring(0, howmany)); //a finished permutation is to be added to "set", - it also removes duplicates.
         //matchWithMap(sofar.toCharArray());
      }
      for (int i = 0; i < input.length(); i++) {
         char c = input.charAt(i);
         if (input.indexOf(c, i + 1) != -1)
            continue;
         permutation(input.substring(0, i) + input.substring(i + 1), sofar + c, howmany);
      }
   }


   /**
    * Function for checking the validity of our input string;
    * @param str
    */
   public static void isValidInput (String str) {
      if (str.isEmpty()) throw new RuntimeException("Entered string was empty!");

      String[] check = str.trim().split("\\s+");
      if (check.length > 3) throw new RuntimeException("Too many words in expression " + str + " !");

      for (String s : check) {
         if (s.length() > 18) throw new RuntimeException("The word " + s + " was more than 18 letters in expression " + str + ", could not solve puzzle!");
      }

      String testvalidity = str.replaceAll("\\s","");
      char[] chars = testvalidity.toCharArray();
      for (char c : chars) {
         if(!Character.isLetter(c) || !Character.isUpperCase(c)) {
            throw new RuntimeException("Entered string " +str+ " included non letter characters or was not uppercase!");
         }
      }
   }
}

